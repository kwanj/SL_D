# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-06 08:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0016_shopping_public'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shopping',
            name='created',
        ),
        migrations.AlterField(
            model_name='shopping',
            name='public',
            field=models.BooleanField(default=False),
        ),
    ]
