# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-05 12:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0014_auto_20171105_0416'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shopping',
            name='public',
        ),
        migrations.AlterField(
            model_name='shopping',
            name='content',
            field=models.TextField(help_text='seperate with commas'),
        ),
        migrations.AlterField(
            model_name='shopping',
            name='name',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
