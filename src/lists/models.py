from __future__ import unicode_literals
from django.conf import settings
from django.db import models
from django.db.models.signals import pre_save,post_save
from django.core.urlresolvers import reverse

from .utils import unique_slug_generator

User 	= settings.AUTH_USER_MODEL

class Shopping(models.Model):
	owner 		= models.ForeignKey(User)
	name		= models.CharField(max_length=200,null=True,blank=True)
	category	= models.CharField(max_length=200,null=True,blank=True) 
	content		= models.TextField(help_text='seperate with commas', null=False, blank=False)
	timestamp	= models.DateTimeField(auto_now_add=True)
	updated		= models.DateTimeField(auto_now=True)
	slug		= models.SlugField(null=True,blank=True)
	public 		= models.BooleanField(default=True)

	class Meta:
		ordering =['-updated', '-timestamp']


	def __str__(self):
		return self.name

	def get_content(self):
		return self.content.split(",")

		
	def get_absolute_url(self):
		return reverse('list-detail', kwargs={'slug':self.slug})

	@property
	def title(self):
		return self.name

def s_pre_save_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = unique_slug_generator(instance)




pre_save.connect(s_pre_save_receiver, sender=Shopping)


