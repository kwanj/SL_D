from django.conf.urls import url
from lists.views import (
	ShoppingListView,
	ShoppingDetailView,
	ShoppingCreateView,
	ShoppingUpdateView
	)

urlpatterns = [

    url(r'^$',ShoppingListView.as_view(),name='lists'),
    url(r'^create/$',ShoppingCreateView.as_view(),name='create'),
    url(r'^(?P<slug>[\w-]+)/$',ShoppingUpdateView.as_view(),name='list-detail'),
    
    
]