from django import forms
from django.forms import ModelForm
from .models import Shopping

class ShoppingCreateForm(forms.Form):
	name		= forms.CharField(required=False)
	category	= forms.CharField(required=True)
	content		= forms.CharField(required=True)


class ShoppingListCreateForm(ModelForm):

	class Meta:
		model 	= Shopping
		fields	= ['name', 'category', 'content', 'public', 'slug'] 