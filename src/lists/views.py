from __future__ import unicode_literals
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import HttpResponse,HttpResponseRedirect
from django.shortcuts import render,get_object_or_404
from django.views.generic import View,ListView,DetailView,CreateView,UpdateView
from .models import Shopping
from .forms import ShoppingListCreateForm



class HomeListView(LoginRequiredMixin, ListView):
	template_name = 'lists/shopping_list.html'
	def get_queryset(self):
		slug = self.kwargs.get('slug')
		if slug:
			queryset 	= Shopping.objects.filter(
					Q(category__icontains=slug) |
					Q(category__iexact=slug)
				)
		else:
			queryset 	= Shopping.objects.all()
		return queryset


class ShoppingListView(LoginRequiredMixin, ListView):
	template_name = 'profiles/user-detail.html'
	def get_queryset(self):
		return Shopping.objects.filter(owner=self.request.user)

class ShoppingDetailView(LoginRequiredMixin, DetailView):
	def get_queryset(self):
		return Shopping.objects.filter(owner=self.request.user)

class ShoppingCreateView(LoginRequiredMixin, CreateView):
	form_class 		=  ShoppingListCreateForm
	template_name 	=  'form.html'
	def form_valid(self, form):
		instance 		= form.save(commit=False)
		instance.owner 	= self.request.user
		return super (ShoppingCreateView, self).form_valid(form)

	def get_context_data(self, *args, **kwargs):
		context 			= super(ShoppingCreateView, self).get_context_data(*args, **kwargs)
		context['title'] 	= 'AddList'
		return context



class ShoppingUpdateView(LoginRequiredMixin, UpdateView):
	form_class 		= ShoppingListCreateForm
	template_name 	= 'lists/detail-update.html'

	def get_queryset(self):
		return Shopping.objects.filter(owner=self.request.user)

	def form_valid(self, form):
		instance 		= form.save(commit=False)
		instance.owner 	= self.request.user
		return super (ShoppingUpdateView, self).form_valid(form)


	def get_context_data(self, *args, **kwargs):
		context 			= super(ShoppingUpdateView, self).get_context_data(*args, **kwargs)
		context['title'] 	= 'ListUpdate'
		return context


	

