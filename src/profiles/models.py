# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings


User = settings.AUTH_USER_MODEL

class Profile(models.Model):
	user 			= models.OneToOneField(User)
	activated 		= models.BooleanField(default=True)
	timestamp		= models.DateTimeField(auto_now_add=True)
	updated			= models.DateTimeField(auto_now=True)


	def __str__(self):
		return self.user.username
