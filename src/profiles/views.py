# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth import get_user_model
from django.http import Http404

from django.shortcuts import render,get_object_or_404,redirect
from django.views.generic import DetailView,CreateView
from .forms import RegisterForm
User 	= get_user_model()

class ProfileDetailView(DetailView):
	template_name = 'profiles/user-detail.html'
	queryset 	  =  User.objects.filter(is_active=True)

	def get_object(self):
		username = self.kwargs.get('username')
		if username is None:
			raise Http404
		return get_object_or_404(User, username__iexact=username, is_active=True)


class RegisterView(CreateView):
	form_class 		= RegisterForm
	template_name 	= 'registration/register.html'
	success_url 	= '/login'


	def dispatch(self, *args, **kwargs):
		return super (RegisterView, self).dispatch(*args, **kwargs)