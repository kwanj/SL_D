from django.conf.urls import url,include
from django.contrib import admin
from django.views.generic import TemplateView
from django.contrib.auth.views import LoginView,PasswordChangeView,LogoutView
from django.contrib.auth import views as auth_views
from lists.views import HomeListView
from profiles.views import RegisterView


urlpatterns = [
	url(r'^admin/', admin.site.urls),
	url(r'^recent/$',HomeListView.as_view(),name='recent'),
    url(r'^lists/', include('lists.urls')),
    url(r'^u/', include('profiles.urls')),
    url(r'^login/$',LoginView.as_view(),name='login'),
     url(r'^logout/$',LogoutView.as_view(),name='logout'),
    url(r'^register/$',RegisterView.as_view(),name='register'),
    url(r'^password_reset/$',PasswordChangeView.as_view(),name='password_reset'),
    url(r'^$',TemplateView.as_view(template_name='home.html'),name='home'),
    url(r'^contact$',TemplateView.as_view(template_name='contact.html'),name='contact'),

]